angular.module('moduleExample', [])
  .controller('UserController', UserController)
  .controller('CountController', CountController);

function UserController($scope, $rootScope) {
  this.user = {
    name: undefined,
    surname: 'Novák',
    yearOfBirth: 1982
  };

  var getYearWatcher_ = function() {
    return this.user.yearOfBirth;
  }.bind(this);

  var broadcastChange_ = function(newValue, oldValue) {
    console.log('Year of birth has changed:', newValue, oldValue);
    $rootScope.$broadcast("yearOfBirth:changed");
  };

  $scope.$watch(getYearWatcher_, broadcastChange_);

  this.getAge = function() {
    console.log('getAge');
    var now = new Date();
    return now.getFullYear() - this.user.yearOfBirth;
  };

  this.clear = function() {
    this.user = {};
  };
}

function CountController($scope) {
  this.count = 0;

  $scope.$on("yearOfBirth:changed", function(event, data) {
    console.log("yearOfBirth:changed");
    this.count++;
  }.bind(this));
}
