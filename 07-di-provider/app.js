'use strict';

// TODO 7 přesuňte služby do samostatného modulu
angular.module('diApp', [])

  .value('defaultProduct', {
    pageSize: 'A5',
    numberOfPages: 50
  })

  // TODO 6 - použití konstanty
  // TODO 4 - konfigurace provideru

  // TODO 2 - přepište factory calculator na provider
  .factory('calculator', calculator)
  .controller('CalculatorCtrl', CalculatorController);

function calculator($log) {
  return {
    getPrice: function(product) {
      // TODO 3 - přesun do provideru a přidání setteru
      var pagePrice = 3;

      var price = 0;
      var baseCoverPrice = 70;
      switch (product.pageSize) {
        case 'A6':
          price += baseCoverPrice;
          break;
        case 'A5':
          price += baseCoverPrice + 20;
          break;
        case 'A4':
          price += baseCoverPrice + 40;
          break;
      }

      var pagesPrice = Math.ceil(product.numberOfPages / 5) * pagePrice;
      price += pagesPrice;

      $log.log('Price : ' + price + " - " + product.numberOfPages + " stran, " + product.pageSize);
      return price;
    }
  };
}

function CalculatorController(defaultProduct, calculator) {
  this.product = defaultProduct;

  this.getPrice = function() {
    return calculator.getPrice(this.product);
  };
}
