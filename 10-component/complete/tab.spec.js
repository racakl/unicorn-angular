describe('Tab component', function(){
  beforeEach(module('componentApp', 'componentApp.templates'));

  beforeEach(inject(function($rootScope, $compile, $templateCache, $log) {

    console.log(angular.module('componentApp.templates'));

    this.$compile = $compile;
    this.$rootScope = $rootScope;

    this.$scope = this.$rootScope.$new();

    this.setTemplate = function(template) {
      this.element = angular.element('<tabset>' + template + '</tabset>');
    };

    this.render = function() {
      var template = this.$compile(this.element)(this.$scope);
      this.$scope.$digest();

      return template;
    };

    this.setTemplate('<tab header="tabName">tab content</tab>');
  }));

  it('shows its content', function(){
    expect(this.render().html()).toMatch('tab content');
  });

  describe('when deactivated', function() {
    beforeEach(function(){

      this.template = this.render();
      this.controller = this.template.find('tab').controller('tab');

      this.controller.active = false;
      this.$scope.$digest();
    });

    it('it hides', function(){
      expect(this.controller.active).toBeFalsy();
      expect(this.template.html()).not.toMatch('tab content');
    });

    it('it can be activated again', function(){
      this.controller.active = true;
      this.$scope.$digest();

      expect(this.controller.active).toBeTruthy();
      expect(this.template.html()).toMatch('tab content');
    });
  });

});
